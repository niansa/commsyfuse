#! /usr/bin/env python3
import subprocess
import os, sys
import requests, json

# Define a little write() helper
def fwrite(path:str, data, mode="w"):
    with open(path, mode=mode) as f:
        f.write(data)

# Check argv
if len(sys.argv) < 3:
    os.write(2, f"Usage:  {argv[0]} <destpath> <SID> <room>\n")
    sys.exit(1);
destpath = sys.argv[1]

# Try accessing/writing to given path
os.listdir(destpath)
fwrite(destpath + "/rwtest", "It works!")
os.remove(destpath + "/rwtest")

# Run program
proc = subprocess.Popen([os.path.dirname(os.path.abspath(__file__))+'/bin/CommSyFuse', 'json'] + sys.argv[2:], stdout=subprocess.PIPE)

# Generate folder/file structure
os.chdir(destpath)
for entry in json.load(proc.stdout):
    # Create and switch to new directory
    os.mkdir(entry["name"])
    os.chdir(entry["name"])
    # Write description
    fwrite("info.txt", entry["meta"] + "\n\n" + entry["description"])
    # Download files
    for file in entry["files"]:
        print("Downloading:", file["url"])
        fwrite(file["name"], requests.get(file["url"], cookies=dict(SID=sys.argv[2])).content, mode="wb")
    # Switch back
    os.chdir("..")
